﻿using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System;
using System.Diagnostics;
using Plugin.Media.Abstractions;
using System.IO;
using MonkeyCache.SQLite;
using Plugin.Connectivity;

namespace Fourplaces
{
    class InfoLoader
    {
        
        public async Task<List<PlaceItemSummary>> GetPlaces()
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                HttpClient client = new HttpClient();
                HttpResponseMessage responseMessage = await client.GetAsync("https://td-api.julienmialon.com/places");
                var content = await responseMessage.Content.ReadAsStringAsync();
                if (Barrel.Current.Exists("listeLieu"))
                {
                    Barrel.Current.Empty("listeLieu");
                    Barrel.Current.Add("listeLieu", content, TimeSpan.FromDays(7));
                }
                else
                {
                    Barrel.Current.Add("listeLieu", content, TimeSpan.FromDays(7));
                }
                Response<List<PlaceItemSummary>> placeItems = JsonConvert.DeserializeObject<Response<List<PlaceItemSummary>>>(content);
                List<PlaceItemSummary> place = placeItems.Data;
                return place;
            }
            else
            {
                var content = Barrel.Current.Get<string>("listeLieu");
                Response<List<PlaceItemSummary>> placeItems = JsonConvert.DeserializeObject<Response<List<PlaceItemSummary>>>(content);
                List<PlaceItemSummary> place = placeItems.Data;
                return place;
            }
        }

       

        public async Task<PlaceItem> GetDetailPlace(int id)
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage responseMessage = await client.GetAsync("https://td-api.julienmialon.com/places/"+id);
            var content = await responseMessage.Content.ReadAsStringAsync();
            Response<PlaceItem> placeItem = JsonConvert.DeserializeObject<Response<PlaceItem>>(content);
            PlaceItem place = placeItem.Data;
            return place;
        }

        public async Task<LoginResult> PostRegisterRequest(RegisterRequest registerRequest)
        {
            HttpClient client = new HttpClient();
            string request = JsonConvert.SerializeObject(registerRequest);
            HttpContent content = new StringContent(request, Encoding.UTF8, "application/json");
            HttpResponseMessage responseMessage = await client.PostAsync("https://td-api.julienmialon.com/auth/register", content);
            var resp = await responseMessage.Content.ReadAsStringAsync();
            Response<LoginResult> tokenResp = JsonConvert.DeserializeObject<Response<LoginResult>>(resp);
            LoginResult token = tokenResp.Data;
            return token;
        }

        public async Task<string> PatchPassword(UpdatePasswordRequest updateRequest)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", App.token.AccessToken);
            string request = JsonConvert.SerializeObject(updateRequest);
            HttpContent content = new StringContent(request, Encoding.UTF8, "application/json");
            HttpRequestMessage messageRequest = new HttpRequestMessage(new HttpMethod("PATCH"), "https://td-api.julienmialon.com/me/password");
            messageRequest.Content = content;
            HttpResponseMessage messageReponse = await client.SendAsync(messageRequest);
            var resp = await messageRequest.Content.ReadAsStringAsync();
            Response response = JsonConvert.DeserializeObject<Response>(resp);
            if (response.IsSuccess)
            {
                return updateRequest.NewPassword;
            }
            else
            {
                return null;
            }
        }


        public async Task<UserItem> PatchProfil(UpdateProfileRequest updateRequest)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", App.token.AccessToken);
            string request = JsonConvert.SerializeObject(updateRequest);
            HttpContent content = new StringContent(request, Encoding.UTF8, "application/json");
            HttpRequestMessage messageRequest = new HttpRequestMessage(new HttpMethod("PATCH"), "https://td-api.julienmialon.com/me");
            messageRequest.Content = content;
            HttpResponseMessage messageReponse = await client.SendAsync(messageRequest);
            var resp = await messageReponse.Content.ReadAsStringAsync();
            Response<UserItem> response = JsonConvert.DeserializeObject<Response<UserItem>>(resp);
            UserItem userItem = response.Data;
            if (userItem!=null)
            { 
                return userItem;
            }
            else
            {
                return null;
            }
        }

        public async Task<int?> PostImage(MediaFile file)
        {            
            HttpClient client = new HttpClient();
            byte[] test = File.ReadAllBytes(file.Path);

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "https://td-api.julienmialon.com/images");
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", App.token.AccessToken);

            MultipartFormDataContent requestContent = new MultipartFormDataContent();

            var imageContent = new ByteArrayContent(test);
            imageContent.Headers.ContentType = MediaTypeHeaderValue.Parse("image/jpeg");

            
            requestContent.Add(imageContent, "file", "file.jpg");

            request.Content = requestContent;

            HttpResponseMessage response = await client.SendAsync(request);


            var resp = await response.Content.ReadAsStringAsync();
            Response<CreateImageRequest> responseItem = JsonConvert.DeserializeObject<Response<CreateImageRequest>>(resp);
            int? result = responseItem.Data.imageId;


            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("Image uploaded!");
                
            }
            else
            {
                Debugger.Break();
            }
            return result;
        }


        public async Task<LoginResult> PostLoginRequest(LoginRequest loginRequest)
        {
            HttpClient client = new HttpClient();
            string request = JsonConvert.SerializeObject(loginRequest);
            HttpContent content = new StringContent(request, Encoding.UTF8, "application/json");
            HttpResponseMessage responseMessage = await client.PostAsync("https://td-api.julienmialon.com/auth/login", content);
            var resp = await responseMessage.Content.ReadAsStringAsync();
            
            Response<LoginResult> tokenResp = JsonConvert.DeserializeObject<Response<LoginResult>>(resp);
            if (tokenResp.IsSuccess)
            {
                LoginResult token = tokenResp.Data;

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.AccessToken);
                responseMessage = await client.GetAsync("https://td-api.julienmialon.com/me");
                var resp2 = await responseMessage.Content.ReadAsStringAsync();
                Response<UserItem> user = JsonConvert.DeserializeObject<Response<UserItem>>(resp2);
                App.profil = user.Data;

                return token;
            }
            else
            {
                return tokenResp.Data;
            }
        }

        public async void PostCommentRequest(CreateCommentRequest commentRequest,int idPlace)
        {
            HttpClient client = new HttpClient();
            string request = JsonConvert.SerializeObject(commentRequest);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", App.token.AccessToken);
            HttpContent content = new StringContent(request, Encoding.UTF8, "application/json");
            HttpResponseMessage responseMessage = await client.PostAsync("https://td-api.julienmialon.com/places/"+idPlace+"/comments", content);
            var resp = await responseMessage.Content.ReadAsStringAsync();
            Response response = JsonConvert.DeserializeObject<Response>(resp);
            if (responseMessage.IsSuccessStatusCode)
            {
                Console.WriteLine("Commentaire uploaded!");
            }
            else
            {
                Debugger.Break();
            }
        }

        public async Task<string> PostPlaceRequest(CreatePlaceRequest placeRequest)
        {
            HttpClient client = new HttpClient();
            string request = JsonConvert.SerializeObject(placeRequest);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", App.token.AccessToken);
            HttpContent content = new StringContent(request, Encoding.UTF8, "application/json");
            HttpResponseMessage responseMessage = await client.PostAsync("https://td-api.julienmialon.com/places", content);
            var resp = await responseMessage.Content.ReadAsStringAsync();
            Response response = JsonConvert.DeserializeObject<Response>(resp);
            if (responseMessage.IsSuccessStatusCode)
            {
                Console.WriteLine("place uploaded!");
            }
            else
            {
                Debugger.Break();
            }
            return null;
        }

    }
}
