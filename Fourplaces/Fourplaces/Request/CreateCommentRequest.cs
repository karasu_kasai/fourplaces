using Newtonsoft.Json;

namespace Fourplaces
{
	public class CreateCommentRequest
	{
		[JsonProperty("text")]
		public string Text { get; set; }
	}
}