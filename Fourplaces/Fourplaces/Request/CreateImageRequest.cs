﻿using Newtonsoft.Json;

namespace Fourplaces
{
    class CreateImageRequest
    {
        [JsonProperty("id")]
        public int? imageId { get; set; }
    }
}
