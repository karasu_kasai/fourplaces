using Newtonsoft.Json;

namespace Fourplaces
{
	public class PlaceItemSummary
	{
        private int _imageId;

        [JsonProperty("id")]
		public int Id { get; set; }
		
		[JsonProperty("title")]
		public string Title { get; set; }
		
		[JsonProperty("description")]
		public string Description { get; set; }
		
		[JsonProperty("image_id")]
        public int ImageId
        {
            get => _imageId;
            set
            {
                _imageId = value;
                ImageSrc = "https://td-api.julienmialon.com/images/" + _imageId;
            }
        }

        [JsonProperty("latitude")]
		public double Latitude { get; set; }
		
		[JsonProperty("longitude")]
		public double Longitude { get; set; }


        public string ImageSrc { get; set; }
    }
}