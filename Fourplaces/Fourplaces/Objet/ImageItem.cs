using Newtonsoft.Json;

namespace Fourplaces
{
	public class ImageItem
	{
		[JsonProperty("id")]
		public int Id { get; set; }
	}
}