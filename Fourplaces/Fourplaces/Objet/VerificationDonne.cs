﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fourplaces
{
    class VerificationDonne
    {

        public bool ChechOk(params object[] elementAVerif)
        {
            foreach(object o in elementAVerif)
            {
                if (o is string test)
                {
                    if (string.IsNullOrEmpty(test))
                    {
                        return false;
                    }
                }
                else
                {
                    if (o.Equals(null))
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
