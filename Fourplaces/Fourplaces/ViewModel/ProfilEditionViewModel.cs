﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;


namespace Fourplaces
{
    class ProfilEditionViewModel : ViewModelBase
    {
        private VerificationDonne verification = new VerificationDonne();
        private int? idImage = null;
        public INavigation Navigation { get; set; }
        private ICommand _OnButtonValiderClicked;
        private ICommand _OnButtonRetourClicked;
        private ICommand _OnButtonImageProfilClicked;
        private string _actualFirstName ="";
        private string _newFirstName = "";
        private string _actualLastName = "";
        private string _newLastName = "";
        private bool _enabImg = true;
        private bool _enabVal = true;
        private bool _enabRet = true;

        public bool EnabImg
        {
            get => _enabImg;
            set => SetProperty(ref _enabImg, value);
        }

        public bool EnabVal
        {
            get => _enabVal;
            set => SetProperty(ref _enabVal, value);
        }

        public bool EnabRet
        {
            get => _enabRet;
            set => SetProperty(ref _enabRet, value);
        }

        public string ActualFirstName
        {
            get => _actualFirstName;
            set => SetProperty(ref _actualFirstName, value);
        }

        public string NewFirstName
        {
            get => _newFirstName;
            set => SetProperty(ref _newFirstName, value);
        }

        public string NewLastName
        {
            get => _newLastName;
            set => SetProperty(ref _newLastName, value);
        }

        public string ActualLastName
        {
            get => _actualLastName;
            set => SetProperty(ref _actualLastName, value);
        }

        public ICommand OnButtonRetourClicked
        {
            get => _OnButtonRetourClicked;
            set => SetProperty(ref _OnButtonRetourClicked, value);
        }

        public ICommand OnButtonValiderClicked
        {
            get => _OnButtonValiderClicked;
            set => SetProperty(ref _OnButtonValiderClicked, value);
        }

        public ICommand OnButtonImageProfilClicked
        {
            get => _OnButtonImageProfilClicked;
            set => SetProperty(ref _OnButtonImageProfilClicked, value);
        }


        public ProfilEditionViewModel(INavigation navigation)
        {
            this.Navigation = navigation;
            OnButtonValiderClicked = new Command(Validation);
            OnButtonRetourClicked = new Command(Retour);
            OnButtonImageProfilClicked = new Command(PickImage);
            ActualFirstName = App.profil.FirstName;
            ActualLastName = App.profil.LastName;
        }

        private async void PickImage()
        {
            EnabImg = false;
            var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
            {
                PhotoSize = PhotoSize.Medium
            });
            if (file != null)
            {               
                InfoLoader infoLoader = new InfoLoader();
                idImage = await infoLoader.PostImage(file);
                App.profil.ImageId = idImage;
            }
            EnabImg = true;
        }

        private async void Validation()
        {
            if (verification.ChechOk(NewFirstName, NewLastName))
            {
                EnabVal = false;
                UpdateProfileRequest profileRequest = new UpdateProfileRequest
                {
                    FirstName = NewFirstName,
                    LastName = NewLastName,
                    ImageId = idImage
                };
                if (idImage.Equals(null))
                {
                    profileRequest.ImageId = App.profil.ImageId;
                }
                InfoLoader infoLoader = new InfoLoader();
                var result = await infoLoader.PatchProfil(profileRequest);
                if (result != null)
                {
                    App.profil = result;
                }
                GoPageProfil();
            }
        }

        private void Retour()
        {
            EnabRet = false;
            GoPageProfil();
        }

        private async void GoPageProfil()
        {
            Navigation.InsertPageBefore(new PageProfil(), Navigation.NavigationStack[Navigation.NavigationStack.Count - 1]);
            await Navigation.PopAsync();
        }
    }
}
