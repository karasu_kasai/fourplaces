﻿using System;
using System.Collections.Generic;
using System.Text;
using Storm.Mvvm;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using System.Windows.Input;
using MonkeyCache.SQLite;
using Plugin.Connectivity;

namespace Fourplaces
{
    class ListeLieuViewModel : ViewModelBase
    {
        private List<PlaceItemSummary> _lieus;
       
        public INavigation Navigation { get; set; }

        public ObservableCollection<PlaceItemSummary> Lieu { get; set; }

        private PlaceItemSummary _selectLieu;

        private ICommand _buttonProfil;
        private ICommand _buttonAddNewPlace;
        

        public ICommand ButtonAddNewPlace
        {
            get => _buttonAddNewPlace;
            set => SetProperty(ref _buttonAddNewPlace, value);
        }

        public ICommand ButtonProfil
        {
            get => _buttonProfil;
            set => SetProperty(ref _buttonProfil, value);
        }

        override
        public async Task OnResume()
        {
            InfoLoader infoLoader = new InfoLoader();
            Lieus = await infoLoader.GetPlaces();            
        }

        public List<PlaceItemSummary> Lieus
        {
            get => _lieus;
            set => SetProperty(ref _lieus, value);
        }

        
        public PlaceItemSummary SelectLieu
        {
            get => _selectLieu;
            set
            {
                _selectLieu = value;
                GoDetailPage();
            }
        }

        public ListeLieuViewModel(INavigation navigation)
        {
            this.Navigation = navigation;
            ButtonProfil = new Command(ProfilClicked);
            ButtonAddNewPlace = new Command(NewPlaceClicked);

        }


        async private void ProfilClicked()
        {
            if (App.launchAppWithInternet)
                await Navigation.PushAsync(new PageProfil());
        }

        async private void NewPlaceClicked()
        {
            if (App.launchAppWithInternet)
            {
                Navigation.InsertPageBefore(new PageAjoutLieu(), Navigation.NavigationStack[Navigation.NavigationStack.Count - 1]);
                await Navigation.PopAsync();
            }
        }

        private async void GoDetailPage()
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                await Navigation.PushAsync(new PageDetails(_selectLieu.Id));
            }
            else
            {
                await Navigation.PushAsync(new PageDetails(_selectLieu));
            }
        }
    }
}
