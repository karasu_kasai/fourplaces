﻿using System;
using System.Collections.Generic;
using System.Text;
using Storm.Mvvm;
using Storm.Mvvm.Forms;
using System.Windows.Input;
using Xamarin.Forms;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;

namespace Fourplaces
{
    class ConnexionViewModel : ViewModelBase
    {
        private VerificationDonne verification = new VerificationDonne();
        private ICommand _OnButtonConnexionClicked;
        private ICommand _OnButtonIncrisptionClicked;
        private string _email = "";
        private string _password = "";
        public INavigation Navigation { get; set; }
        private bool _visible = true;
        private bool _visibleI = true;

        public bool Visible
        {
            get => _visible;
            set => SetProperty(ref _visible, value);
        }

        public bool VisibleI
        {
            get => _visibleI;
            set => SetProperty(ref _visibleI, value);
        }

        
        public ICommand OnButtonIncrisptionClicked
        {
            get => _OnButtonIncrisptionClicked;
            set => SetProperty(ref _OnButtonIncrisptionClicked, value);
        }

        public ICommand OnButtonConnexionClicked
        {
            get => _OnButtonConnexionClicked;
            set => SetProperty(ref _OnButtonConnexionClicked, value);
        }

        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        public ConnexionViewModel(INavigation navigation)
        {
            this.Navigation = navigation;
            OnButtonConnexionClicked = new Command(Connexion);
            OnButtonIncrisptionClicked = new Command(Inscription);
        }

        private void Connexion()
        {
            GoMainPage();
        }

        private void Inscription()
        {
            GoInscriptionPage();
        }

        private async void GoMainPage()
        {
            if (verification.ChechOk(Email, Password))
            {
                Visible = false;
                LoginRequest loginRequest = new LoginRequest
                {
                    Email = Email,
                    Password = Password
                };
                InfoLoader loader = new InfoLoader();
                var result = await loader.PostLoginRequest(loginRequest);
                if (result!=null)
                {
                    App.token = result;
                    if (App.token != null)
                    {
                        await Navigation.PushAsync(new MainPage());
                        Visible = true;
                    }
                }
            }
        }

        private async void GoInscriptionPage()
        {
            VisibleI = false;
            await Navigation.PushAsync(new PageInscription());
            VisibleI = true;
        }

       

    }
}
