﻿using Storm.Mvvm;
using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace Fourplaces
{
    class PageEditionMdpViewModel : ViewModelBase
    {
        private VerificationDonne verification = new VerificationDonne();
        private ICommand _OnButtonValiderClicked;
        private ICommand _OnButtonRetourClicked;
        private string _oldPassword="";
        private string _newPassword="";
        public INavigation Navigation { get; set; }
        private bool _enabVal = true;
        private bool _enabRet = true;

        public bool EnabVal
        {
            get => _enabVal;
            set => SetProperty(ref _enabVal, value);
        }

        public bool EnabRet
        {
            get => _enabRet;
            set => SetProperty(ref _enabRet, value);
        }

        public string OldPassword
        {
            get => _oldPassword;
            set => SetProperty(ref _oldPassword, value);
        }

        public string NewPassword
        {
            get => _newPassword;
            set => SetProperty(ref _newPassword, value);
        }

        public ICommand OnButtonRetourClicked
        {
            get => _OnButtonRetourClicked;
            set => SetProperty(ref _OnButtonRetourClicked, value);
        }

        public ICommand OnButtonValiderClicked
        {
            get => _OnButtonValiderClicked;
            set => SetProperty(ref _OnButtonValiderClicked, value);
        }


        public PageEditionMdpViewModel(INavigation navigation)
        {
            this.Navigation = navigation;
            OnButtonValiderClicked = new Command(Validation);
            OnButtonRetourClicked = new Command(Retour);
        }

        private async void Validation()
        {
            if (verification.ChechOk(OldPassword, NewPassword))
            {
                EnabVal = false;
                UpdatePasswordRequest updateRequest = new UpdatePasswordRequest
                {
                    OldPassword = this.OldPassword,
                    NewPassword = this.NewPassword
                };
                InfoLoader infoLoader = new InfoLoader();
                var result = await infoLoader.PatchPassword(updateRequest);
                GoPageProfil();
            }
        }

        private void Retour()
        {
            EnabRet = false;
            GoPageProfil();
        }

        private void GoPageProfil()
        {
            Navigation.PopAsync();
        }
    }
}