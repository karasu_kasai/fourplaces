﻿using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;

namespace Fourplaces
{
    class AjoutLieuViewModel : ViewModelBase
    {
        private VerificationDonne verification = new VerificationDonne();
        private IGeolocator geolocator = CrossGeolocator.Current;
        public INavigation Navigation { get; set; }
        private int? idImage = null;
        private string _title ="";
        private string _description="";
        private string _latitude="";
        private string _longitude="";
        private ICommand _OnButtonValiderClicked;
        private ICommand _OnButtonRetourClicked;
        private ICommand _OnButtonImageLieuClicked;
        private ICommand _OnButtonTakePhotoClicked;
        private bool _enabImg = true;
        private bool _enabPho = true;
        private bool _enabVal = true;
        private bool _enabRet = true;

        public bool EnabImg
        {
            get => _enabImg;
            set => SetProperty(ref _enabImg, value);
        }

        public bool EnabPho
        {
            get => _enabPho;
            set => SetProperty(ref _enabPho, value);
        }

        public bool EnabVal
        {
            get => _enabVal;
            set => SetProperty(ref _enabVal, value);
        }

        public bool EnabRet
        {
            get => _enabRet;
            set => SetProperty(ref _enabRet, value);
        }

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        public string Description
        {
            get => _description;
            set => SetProperty(ref _description, value);
        }

        public string Latitude
        {
            get => _latitude;
            set => SetProperty(ref _latitude, value);
        }

        public string Longitude
        {
            get => _longitude;
            set => SetProperty(ref _longitude, value);
        }

        public ICommand OnButtonRetourClicked
        {
            get => _OnButtonRetourClicked;
            set => SetProperty(ref _OnButtonRetourClicked, value);
        }

        public ICommand OnButtonValiderClicked
        {
            get => _OnButtonValiderClicked;
            set => SetProperty(ref _OnButtonValiderClicked, value);
        }

        public ICommand OnButtonImageLieuClicked
        {
            get => _OnButtonImageLieuClicked;
            set => SetProperty(ref _OnButtonImageLieuClicked, value);
        }

        public ICommand OnButtonTakePhotoClicked
        {
            get => _OnButtonTakePhotoClicked;
            set => SetProperty(ref _OnButtonTakePhotoClicked, value);
        }

        public AjoutLieuViewModel(INavigation navigation)
        {
            Navigation = navigation;
            GetLocationActu();
            OnButtonImageLieuClicked = new Command(PickImage);
            OnButtonRetourClicked = new Command(Retour);
            OnButtonValiderClicked = new Command(Validation);
            OnButtonTakePhotoClicked = new Command(TakePhoto);
        }

        
        private async void GetLocationActu()
        {
            Position position = await geolocator.GetPositionAsync();
            Latitude = position.Latitude.ToString("F6");
            Longitude = position.Longitude.ToString("F6");
        }

        private async void PickImage()
        {
            EnabImg = false;
            var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions {
                PhotoSize = PhotoSize.Small
            });
            if (file != null)
            {
                var path = file.Path;
                System.Diagnostics.Debug.WriteLine(path);
                InfoLoader infoLoader = new InfoLoader();
                idImage = await infoLoader.PostImage(file);
            }
            EnabImg = true;
        }

        private async void TakePhoto()
        {
            EnabPho = false;
            await CrossMedia.Current.Initialize();
            var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                SaveToAlbum = true,
                PhotoSize = PhotoSize.Small
            });           

            if (file != null)
            {
                InfoLoader infoLoader = new InfoLoader();
                idImage = await infoLoader.PostImage(file);
            }
            EnabPho = true;
        }

       

        private async void Validation()
        {
            if (verification.ChechOk(Title, Description, Latitude, Longitude))
            {
                EnabVal = false;
                CreatePlaceRequest placeRequest = new CreatePlaceRequest
                {
                    Title = this.Title,
                    Description = this.Description,
                    ImageId = (int)idImage,
                    Latitude = Double.Parse(this.Latitude.Replace(',', '.'), CultureInfo.InvariantCulture),
                    Longitude = Double.Parse(this.Longitude.Replace(',', '.'), CultureInfo.InvariantCulture)
                };
           
                InfoLoader infoLoader = new InfoLoader();
                var resp = await infoLoader.PostPlaceRequest(placeRequest);
                GoPagePlaceSummary();
            }            
        }



        private void Retour()
        {
            EnabRet = false;
            GoPagePlaceSummary();
        }

        private async void GoPagePlaceSummary()
        {
            Navigation.InsertPageBefore(new MainPage(), Navigation.NavigationStack[Navigation.NavigationStack.Count - 1]);
            await Navigation.PopAsync();
        }
    }
}
