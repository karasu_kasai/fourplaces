﻿using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace Fourplaces
{
    class InscriptionViewModel : ViewModelBase
    {
        private VerificationDonne verification = new VerificationDonne();
        private ICommand _OnButtonInscriptClicked;
        private ICommand _OnButtonRetourClicked;
        private string _email ="";
        private string _firstName="";
        private string _lastName="";
        private string _password="";
        public INavigation Navigation { get; set; }
        private bool _enabVal = true;
        private bool _enabRet = true;

        public bool EnabVal
        {
            get => _enabVal;
            set => SetProperty(ref _enabVal, value);
        }

        public bool EnabRet
        {
            get => _enabRet;
            set => SetProperty(ref _enabRet, value);
        }
       

        public ICommand OnButtonInscriptClicked
        {
            get => _OnButtonInscriptClicked;
            set => SetProperty(ref _OnButtonInscriptClicked, value);
        }

        public ICommand OnButtonRetourClicked
        {
            get => _OnButtonRetourClicked;
            set => SetProperty(ref _OnButtonRetourClicked, value);
        }

        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        public string FirstName
        {
            get => _firstName;
            set => SetProperty(ref _firstName, value);
        }

        public string LastName
        {
            get => _lastName;
            set => SetProperty(ref _lastName, value);
        }

        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }



        public InscriptionViewModel(INavigation navigation)
        {
            this.Navigation = navigation;
            OnButtonInscriptClicked = new Command(Inscription);
            OnButtonRetourClicked = new Command(Retour);
        }

        private async void Inscription()
        {
            if (verification.ChechOk(Email, FirstName, LastName, Password))
            {
                EnabVal = false;
                RegisterRequest register = new RegisterRequest
                {
                    Email = Email,
                    FirstName = FirstName,
                    LastName = LastName,
                    Password = Password
                };
                InfoLoader infoLoader = new InfoLoader();
                var result = await infoLoader.PostRegisterRequest(register);
                App.token = result;
                LoginResult t = App.token;
                GoPageConnexion();
            }
        }

        private void Retour()
        {
            EnabRet = false;
            GoPageConnexion();
        }

        private void GoPageConnexion()
        {        
            Navigation.PopAsync();
        }

    }
}
