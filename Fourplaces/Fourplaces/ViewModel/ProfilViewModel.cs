﻿using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Fourplaces
{
    class ProfilViewModel : ViewModelBase
    {
        private string _imageProfil;
        private string _pseudoProfil;
        private string _firstnameProfil;
        private string _lastnameProfil;
        private ICommand _onButtonEditionProfilClicked;
        private ICommand _onButtonEditionMDPClicked;
        public INavigation Navigation { get; set; }
        private bool _enabPro = true;
        private bool _enabMdp = true;

        public bool EnabPro
        {
            get => _enabPro;
            set => SetProperty(ref _enabPro, value);
        }

        public bool EnabMdp
        {
            get => _enabMdp;
            set => SetProperty(ref _enabMdp, value);
        }

        public string ImageProfil
        {
            get => _imageProfil;
            set => SetProperty(ref _imageProfil, value);
        }

        public string PseudoProfil
        {
            get => _pseudoProfil;
            set => SetProperty(ref _pseudoProfil, value);
        }

        public string FirstnameProfil
        {
            get => _firstnameProfil;
            set => SetProperty(ref _firstnameProfil, value);
        }

        public string LastnameProfil
        {
            get => _lastnameProfil;
            set => SetProperty(ref _lastnameProfil, value);
        }

        public ICommand OnButtonEditionProfilClicked
        {
            get => _onButtonEditionProfilClicked;
            set => SetProperty(ref _onButtonEditionProfilClicked, value);
        }

        public ICommand OnButtonEditionMDPClicked
        {
            get => _onButtonEditionMDPClicked;
            set => SetProperty(ref _onButtonEditionMDPClicked, value);
        }

        public ProfilViewModel(INavigation navigation)
        {
            this.Navigation = navigation;
            if (App.profil.ImageId.Equals(null))
            {
                ImageProfil = "profil.png";
            }
            else
            {
                ImageProfil = "https://td-api.julienmialon.com/images/" + App.profil.ImageId;
            }
            PseudoProfil = App.profil.Email;
            FirstnameProfil = App.profil.FirstName;
            LastnameProfil = App.profil.LastName;
            OnButtonEditionMDPClicked = new Command(GoEditionMDPPage);
            OnButtonEditionProfilClicked = new Command(GoEditionProfilPage);
        }

        private async void GoEditionMDPPage()
        {
            EnabMdp = false;
            await Navigation.PushAsync(new PageEditionMdp());
            EnabMdp = true;
        }

        private async void GoEditionProfilPage()
        {
            EnabPro = false;
            Navigation.InsertPageBefore(new PageEditionProfil(), Navigation.NavigationStack[Navigation.NavigationStack.Count - 1]);
            await Navigation.PopAsync();
        }

    }
}
