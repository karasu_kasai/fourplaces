﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using Storm.Mvvm;

namespace Fourplaces
{
    class AjoutCommentaireViewModel : ViewModelBase
    {
        private VerificationDonne verification = new VerificationDonne();
        public INavigation Navigation { get; set; }
        private string _commentaire ="";
        private int id;
        private ICommand _onButtonPostCommentClicked;
        private ICommand _onButtonRetourPageDetailClicked;
        private bool _enabled1 = true;
        private bool _enabled2 = true;

        public bool Enabled1
        {
            get => _enabled1;
            set => SetProperty(ref _enabled2, value);
        }

        public bool Enabled2
        {
            get => _enabled2;
            set => SetProperty(ref _enabled2, value);
        }

        public string Commentaire
        {
            get => _commentaire;
            set => SetProperty(ref _commentaire, value);
        }

        public ICommand OnButtonPostCommentClicked
        {
            get => _onButtonPostCommentClicked;
            set => SetProperty(ref _onButtonPostCommentClicked, value);
        }

        public ICommand OnButtonRetourPageDetailClicked
        {
            get => _onButtonRetourPageDetailClicked;
            set => SetProperty(ref _onButtonRetourPageDetailClicked, value);
        }

        public AjoutCommentaireViewModel(INavigation navigation,int id)
        {
            this.Navigation = navigation;
            this.id = id;
            OnButtonPostCommentClicked = new Command(PostComment);
            OnButtonRetourPageDetailClicked = new Command(Retour);
        }

        private async void Retour()
        {
            Enabled2 = false;
            Navigation.InsertPageBefore(new PageDetails(id), Navigation.NavigationStack[Navigation.NavigationStack.Count - 1]);
            await Navigation.PopAsync();
        }

        private async void PostComment()
        {
            if (verification.ChechOk(Commentaire))
            {
                Enabled1 = false;
                InfoLoader infoLoader = new InfoLoader();
                CreateCommentRequest commentRequest = new CreateCommentRequest
                {
                    Text = Commentaire
                };
                infoLoader.PostCommentRequest(commentRequest, id);
                Navigation.InsertPageBefore(new PageDetails(id), Navigation.NavigationStack[Navigation.NavigationStack.Count - 1]);
                await Navigation.PopAsync();
            }
        }

    }
}
