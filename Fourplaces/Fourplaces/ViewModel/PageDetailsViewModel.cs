﻿using Plugin.Connectivity;
using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Fourplaces
{
    class PageDetailsViewModel : ViewModelBase
    {
        private int id;
        private Map _map;
        public INavigation Navigation { get; set; }
        private bool _enab = true;

        public bool Enab
        {
            get => _enab;
            set => SetProperty(ref _enab, value);
        }

        private string _summary;
        private string _image;
        private ICommand _onButtonAddCommentClicked;


        private List<CommentItem> _commentaireList;

        public ObservableCollection<CommentItem> CommentaireListe { get; set; }

        override
        public async Task OnResume()
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                InfoLoader infoLoader = new InfoLoader();
                PlaceItem p = await infoLoader.GetDetailPlace(id);
                Image = p.ImageSrc;
                Summary = p.Description;
                CommentaireList = p.Comments;
                Map = InitMap(p.Latitude, p.Longitude, p.Title);
            }
        }

        public List<CommentItem> CommentaireList
        {
            get => _commentaireList;
            set => SetProperty(ref _commentaireList, value);
        }

        public string Summary
        {
            get => _summary;
            set => SetProperty(ref _summary, value);
        }

        public string Image
        {
            get => _image;
            set => SetProperty(ref _image, value);
        }

        
        public Map Map {
            get => _map;
            private set => SetProperty(ref _map,value);
        }

        public ICommand OnButtonAddCommentClicked
        {
            get => _onButtonAddCommentClicked;
            set => SetProperty(ref _onButtonAddCommentClicked, value);
        }


        public PageDetailsViewModel(int idd, INavigation navigation)
        {
            this.Navigation = navigation;
            id = idd;
            OnButtonAddCommentClicked = new Command(CreateComment);
        }

        public PageDetailsViewModel(PlaceItemSummary p, INavigation navigation)
        {
            this.Navigation = navigation;
            Image = p.ImageSrc;
            Summary = p.Description;
            InitMap(p.Latitude, p.Longitude, p.Title);
        }

        private async void CreateComment()
        {
            Enab = false;
            Navigation.InsertPageBefore(new PageAjoutCommentaire(id), Navigation.NavigationStack[Navigation.NavigationStack.Count - 1]);
            await Navigation.PopAsync();
        }

        private Map InitMap(double latitude, double longitude, string titre)
        {
            var position = new Position(latitude, longitude); // Latitude, Longitude
            var map = new Map(
            MapSpan.FromCenterAndRadius(position, Distance.FromMiles(0.3)))
            {
                IsShowingUser = true,
                HeightRequest = 250,
                WidthRequest = 250,
                VerticalOptions = Xamarin.Forms.LayoutOptions.FillAndExpand
            };

            //Map = map;

            var slider = new Slider(1, 18, 1);
            slider.ValueChanged += (sender, e) => {
                var zoomLevel = e.NewValue; // between 1 and 18
                var latlongdegrees = 360 / (Math.Pow(2, zoomLevel));
                map.MoveToRegion(new MapSpan(map.VisibleRegion.Center, latlongdegrees, latlongdegrees));
            };


            var pin = new Pin
            {
                Type = PinType.Place,
                Position = position,
                Label = titre
            };
            map.Pins.Add(pin);

            return map;
        }






    }
}
