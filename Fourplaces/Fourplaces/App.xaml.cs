﻿using MonkeyCache;
using MonkeyCache.SQLite;
using Plugin.Connectivity;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Fourplaces
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();


            Barrel.ApplicationId = "fourplaces";

            if (CrossConnectivity.Current.IsConnected)
            {
                launchAppWithInternet = true;
                MainPage = new NavigationPage(new PageConnexion());
            }
            else
            {
                launchAppWithInternet = false;
                MainPage = new NavigationPage(new MainPage());
            }

           
        }

        public static LoginResult token { get; set; }
        public static UserItem profil { get; set; }
        public static bool launchAppWithInternet { get; set; }

       
        protected override void OnStart()
        {
            // Handle when your app starts
            
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        
    }
}
