﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Fourplaces
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PageEditionMdp : ContentPage
	{
		public PageEditionMdp ()
		{
			InitializeComponent ();
            BindingContext = new PageEditionMdpViewModel(Navigation);
		}
	}
}