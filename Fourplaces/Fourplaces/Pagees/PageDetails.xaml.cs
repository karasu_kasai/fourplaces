﻿using Storm.Mvvm.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace Fourplaces
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PageDetails : BaseContentPage
	{
		
        public PageDetails (int id)
        {
            InitializeComponent();
            BindingContext = new PageDetailsViewModel(id,Navigation);
        }       
        
        public PageDetails(PlaceItemSummary p)
        {
            InitializeComponent();
            BindingContext = new PageDetailsViewModel(p, Navigation);
        }
	}
}