﻿using Storm.Mvvm.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Fourplaces
{
    public partial class MainPage : BaseContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            BindingContext = new ListeLieuViewModel(Navigation);
        }


        public MainPage(String paramater) : this()
        {
            InitializeComponent();
            BindingContext = new ListeLieuViewModel(Navigation);
        }


    }
}

